#!/bin/bash
set -e

##################################################
# 변수 및 함수 정의
##################################################
ECHO_SUCCESS="echo -e \\033[1;32m[SUCCESS]\\033[0;39m"
ECHO_FAILURE="echo -e \\033[1;31m[FAILED]\\033[0;39m"
APP_NAME="ci-cd-exmaple"

INIT() {
    cd $(dirname ${0})
    APP_HOME=$(pwd)
    APP_CONF="${APP_HOME}/conf/${1}/"
    
    if [ -z "${1}" ] ||  [ ! -d "${APP_CONF}" ]; then
        echo "Resource directory(${APP_CONF}) is not valid. Please check the second parameter: \"${1}\""
        exit 1
    fi
    
    JAR_CONDITION="${APP_HOME}/bin/*.jar"
    JAR_NAME=$(find $JAR_CONDITION)
    JAR_COUNT=$(find $JAR_CONDITION | wc -l)
    
    if [ "${JAR_COUNT}" != "1" ]; then
        echo "Multiple jar files exist : ${JAR_NAME}"
        exit 1
    fi
    
    APP_LOG="${APP_HOME}/logs"
    APP_PORT="18443"
    APP_OPTS="-Dapp.home=${APP_HOME} \
                -Dprogram.name=${APP_NAME} \
                -server \
                -Djava.net.preferIPv4Stack=true \
                -Xloggc:${APP_LOG}/gc.log \
                -verbose:gc \
                -XX:+PrintGCDetails \
                -XX:+PrintGCDateStamps \
                -XX:+PrintGCTimeStamps \
                -XX:+UseG1GC \
                -XX:+PrintAdaptiveSizePolicy \
                -XX:+HeapDumpOnOutOfMemoryError \
                -XX:HeapDumpPath=${APP_LOG}"
    return 0
}

P_START() {
    echo ""
    echo -en "Process start: "
    
    PID=$(/bin/ps -o pid,cmd -C java  | grep ${APP_NAME} | /bin/awk '{print $1,$2,$3,$4}')
    CNT=$(/bin/ps -o pid,cmd -C java  | grep ${APP_NAME} | wc -l | /bin/awk '{print $1}')
    
    if [ "${PID}" ] || [ -n "${PID}" ]; then # error
        ${ECHO_FAILURE}
        echo "ALREADY running with process count [ ${CNT} ] "
        exit 1
    fi
    
    # ci-cd-example 관련 환경변수 세팅
    INIT "${1}"
    
    /usr/local/jdk_1.8/bin/java ${APP_OPTS} -jar ${JAR_NAME} --spring.config.location=${APP_CONF} --server.port=${APP_PORT} > /dev/null 2>&1 &
    
    RC=${?}
    PID=${!}
    
    if [ "${RC}" != "0" ]; then # error
        ${ECHO_FAILURE}
        echo "Can't execute <${APP_NAME}>"
        exit 1
    fi
    
    ${ECHO_SUCCESS};
    echo "Process started successfully. PID: ${PID}"
    return 0
}

P_STOP() {
    echo ""
    echo -en "Process stop: "
    
    PID=$(/bin/ps -o pid,cmd -C java  | grep ${APP_NAME} | /bin/awk '{print $1}')
    
    if [ -z "${PID}" ] || [ -z "${PID}" ]; then # error
        ${ECHO_FAILURE}
        echo "${APP_NAME} is NOT running..."
        return 0
    fi
    
    for PS in ${PID}
    do
        kill -SIGTERM ${PS}
    done
    
    RC=${?}
    
    if [ "${RC}" != "0" ]; then # error
        ${ECHO_FAILURE}
        echo "Can't stop <${APP_NAME}> process"
        exit 1
    fi
    
    ${ECHO_SUCCESS};
    echo "Process stopped successfully. PID: ${PID}"
    return 0
}

# 실행 모드에 따른 분기
P_PROCESS() {
    USE_MODE="${1}"
    TARGET_CONF="${2}"
    
    case "${USE_MODE}" in
        "start")
            P_START "${TARGET_CONF}"
            ;;
        "stop")
            P_STOP
            ;;
        "restart")
            P_STOP
            sleep 10s
            P_START "${TARGET_CONF}"
            ;;
        *)
            echo "Invalid mode"
            exit 1
            ;;
    esac
    return 0
}

##################################################
# main routine
##################################################
if [ -z "${1}" ]; then
    echo ""
    echo "  Usage                : run.sh (start svr_name|stop|restart svr_name)>"
    echo "  start svr_name       : ${APP_NAME} start"
    echo "  stop                 : ${APP_NAME} stop"
    echo "  restart svr_name     : ${APP_NAME} restart"
    echo ""
    exit 1
fi

P_PROCESS "${1}" "${2}"