### 1. 프로젝트와 관련해서
-------------------------------------------
java 언어 기준 GitLab CI/CD 예제 프로젝트. 프로젝트의 기술 스택은 아래와 같다. 참고로 이 프로젝트는 GItLab 10.7.3 버전 기준으로 제공하는 기능을 이용한다.
- Language: __java 1.8__
- Build Tool: __maven__
- Framework: __Spring Boot__

#### 1.1 주의 사항
GitLab CI/CD에 대한 예제 프로젝트로 단순한 pipeline을 갖는다. 그렇기 때문에 해당 프로젝트를 CI/CD에 대한 이해를 위해 참고하길 바라며, 실제 프로젝트에 CI/CD를 적용할 때는 알맞은 pipeline 구성과 job 실행에 대한 규칙을 고려해야한다.
뿐만 아니라 GitLac CI/CD는 GitLab의 branch 전략과 직접적으로 연관이 있기 때문에 GitLab CI/CD 도입 시에는 GitLab의 branch 전략도 같이 고려해야 한다.

### 2. GitLab CI/CD와 관련해서
-------------------------------------------
GItLab 10.7.3 CI/CD에 대한 document는 아래 링크를 참고한다. 공식 홈페이지에서 document를 제공하지 않으며 사용자가 호스팅할 수 있도록 docker image를 제공중이다.
- __https://docs.gitlab.com/archives/__

#### 2.1 .gitlab-ci.yml 파일 내 변수
- __BUILD_IMAGE__: maven 빌드 시 사용 할 docker image
- __DEPLOY_IMAGE__: maven 빌드 결과물(artifact)에 대해 서버 배포 시 사용 할 docker image
- __APP_NAME__: 프로젝트 이름(pom.xml 내 name 프로퍼티). 빌드 결과물의 최상위 디렉토리 이름(pom.xml 내 outputDir 프로퍼티)
- __GIT_STRATEGY__: job 내에서 프로젝트를 clone, 또는 fetch, 또는 none(아무것도 하지 않음)할지 결정. GitLab의 내장 변수 
- __STG_DEPLOY_PATH__: 스테이지 서버 내 배포 장소 
- __STG_IP__: 스테이지 서버 ip(파일 업로드를 위한 IP). 프로젝트의 Secret variables에 정의
- __STG_PORT__: 스테이지 서버 port(파일 업로드를 위한 ssh port). 프로젝트의 Secret variables에 정의
- __STG_ACCOUNT__: 스테이지 서버 계정. 프로젝트의 Secret variables에 정의
- __STG_PRIVATE_KEY__: 스테이지 서버 내 등록된 ssh private key. 프로젝트의 Secret variables에 정의
- __STG_HOSTNAME__: run.sh 실행 시, resource 디렉토리 이름

### 3. 기타
-------------------------------------------
- CI/CD monitoring:
- Integrations: 